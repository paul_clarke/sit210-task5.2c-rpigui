import RPi.GPIO as GPIO
import tkinter as tk
import random
import time


class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(10, GPIO.OUT)
        GPIO.setup(8, GPIO.OUT)
        GPIO.setup(7, GPIO.OUT)

        self.master = master
        self.pack()
        self.create_widgets()

    blue_status = 0
    red_status = 3
    green_status = 5


    def create_widgets(self):
        self.green_on_button = tk.Radiobutton(self, text="Green On", variable=self.green_status, value=1, command=self.green_on)
        self.green_off_button = tk.Radiobutton(self, text="Green Off", variable=self.green_status, value=0, command=self.green_off)
        self.green_on_button.grid(row=0, column=0)
        self.green_off_button.grid(row=0, column=3)

        self.blue_on_button = tk.Radiobutton(self, text="Blue On", variable=self.blue_status, value=2, command=self.blue_on)
        self.blue_off_button = tk.Radiobutton(self, text="Blue Off", variable=self.blue_status, value=3, command=self.blue_off)
        self.blue_on_button.grid(row=1, column=0)
        self.blue_off_button.grid(row=1, column=3)

        self.red_on_button = tk.Radiobutton(self, text="Red On", variable=self.red_status, value=4, command=self.red_on)
        self.red_off_button = tk.Radiobutton(self, text="Red Off", variable=self.red_status, value=5, command=self.red_off)
        self.red_on_button.grid(row=2, column=0)
        self.red_off_button.grid(row=2, column=3)

        self.party = tk.Button(self, text="Party Mode", fg="green",
                               command=self.party_mode)
        self.party.grid(row=3, column=2)

        self.quit = tk.Button(self, text="QUIT", fg="red",
                              command=self.quit_program)
        self.quit.grid(row=4, column=2)


    def blue_on(self):
        GPIO.output(8, GPIO.LOW)


    def blue_off(self):
        GPIO.output(8, GPIO.HIGH)


    def green_on(self):
        GPIO.output(10, GPIO.LOW)


    def green_off(self):
        GPIO.output(10, GPIO.HIGH)


    def red_on(self):
        GPIO.output(7, GPIO.LOW)


    def red_off(self):
        GPIO.output(7, GPIO.HIGH)

    def party_mode(self):
        pins = [self.blue_on, self.blue_off, self.green_on, self.green_off, self.red_on, self.red_off]
        for i in range(0,100):
            random.choice(pins)()
            time.sleep(random.randint(0,1)/10)
        self.green_off()
        self.blue_off()
        self.red_off()


    def quit_program(self):
        self.blue_off()
        self.green_off()
        self.red_off()
        GPIO.cleanup()
        self.master.destroy()

root = tk.Tk()
root.title("LED Change")
app = Application(master=root)
app.mainloop()
